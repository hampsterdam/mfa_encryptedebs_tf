#################################
# mfa_encryptedEBS_tf
##############################

locals {
  project_name = "Project"
}

provider "aws" {
  region  = var.aws_default_region
  profile = var.aws_profile
  version = "~> 3"
}

module "user_creation" {
  for_each     = toset(var.user_names)
  source       = "./user-create"
  user_name    = each.value
  cluster_name = var.cluster_name
  project_name = local.project_name
}

module "user_group_assign" {
  source       = "./user-groups"
  users        = var.user_names
  cluster_name = var.cluster_name
  project_name = local.project_name
}

