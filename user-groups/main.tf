############
# user-groups
############

locals {
  common_tags = map(
    "Project", var.project_name,
    "Cluster", var.cluster_name,
    "Module", "user-groups"
  )
}

data "local_file" "group_policy" {
  filename = "${path.module}/group_policy.json"
}

resource "aws_iam_group" "iam_group" {
  name = "${var.cluster_name}"
}

resource "aws_iam_group_policy" "iam_group_policy" {
  name = "${var.cluster_name}-group_policy"
  group = aws_iam_group.iam_group.name
  policy = data.local_file.group_policy.content
}

resource "aws_iam_group_membership" "group_membership" {
  name = "${var.cluster_name}-group-membership"
  users = var.users
  group = aws_iam_group.iam_group.name
}