variable "aws_default_region" {
  description = "Region for IAC infrastruture"
  type        = string
}

variable "aws_profile" {
  description = "Profile to apply resources to"
  type        = string
  default     = "default"
}

variable "cluster_name" {
  description = "Common name for all resources in cluster"
  type        = string
}

variable "my_account_alias" {
  description = "Alias for this AWS account"
  type        = string
}

variable "user_names" {
  description = "List of user names"
  type        = set(string)
}

