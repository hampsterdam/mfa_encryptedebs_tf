variable "user_name" {
  description = "Single user names"
  type        = string
}

variable "project_name" {
  description = "Common name for all resources in project"
  type        = string
}

variable "cluster_name" {
  description = "Common name for all resources in cluster"
  type        = string
}

variable "more_tags" {
  description = "Any additional tags to add"
  type        = map(string)
  default     = {}
}
