# User-Create

This terraform module allows for the creation of users and enforces the use of MFA for login. There is no default password set and must be changed upon first login. Then, the user is unable to perform any functions except add a MFA token, at which point, the user has access to the default resources as scoped in the policy document.

Inputs:
* user_name

Outputs:
* user_name (confirmation)

## Admin Notes
* When creating the user, there is no associated login profile. Thus it is necessary to manually set a first time password for the new user, who then in turn must create a new password and MFA upon first login

## Dev Notes
* Once a user has added their MFA, if the changes do not immediately apply, log out and log back in
* The created user only has initial access to EC2, S3, and Cloudwatch resources, and is not attached to a group, role, or policy. Future work should edit the `mfa_policy.json` to scope what an initial permission set should be.
* When a user is deleted via `terraform destory`, for now we must first remove the MFA resource manually first since the MFA was added outside of terraform's configuration and terraform should only manage resources it creates. In theory, this can be automated by provisioners on destry to first list MFA devices, take each MFA serial number, deactivate the MFA, then delete the MFA
