############
# user-create
############

locals {
  common_tags = map(
    "Project", var.project_name,
    "Cluster", var.cluster_name,
    "Module", "user-create"
  )
}


data "local_file" "iam_policy" {
  filename = "${path.module}/mfa_policy.json"
}

resource "aws_iam_user" "base_user" {
  name          = var.user_name
  path          = "/"
  force_destroy = true
  
  tags = merge(
    local.common_tags,
    var.more_tags,
    map(
      "Name", var.user_name
    )
  )
}

resource "aws_iam_user_policy" "mfa_requirement" {
  # checkov:skip=CKV_AWS_40:Users should have MFA regardless of group membership
  name   = "require_mfa"
  user   = aws_iam_user.base_user.name
  policy = data.local_file.iam_policy.content
}

